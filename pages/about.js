import React from "react";

export default function about() {
  if (typeof window !== "undefined") {
    localStorage.setItem("rout", "/about");
  }
  return (
    <>
      <div className="mobile__sidebar">
        <div className="close">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30.406"
            height="30.406"
            viewBox="0 0 30.406 30.406"
          >
            <g
              id="Group_28"
              data-name="Group 28"
              transform="translate(-216.297 -8.672)"
            >
              <path
                id="Path_13"
                data-name="Path 13"
                d="M278,7h35"
                transform="translate(27.5 -190.025) rotate(45)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
              <path
                id="Path_14"
                data-name="Path 14"
                d="M0,0H35"
                transform="translate(243.874 11.5) rotate(135)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
            </g>
          </svg>
        </div>
      </div>
      <section className="about">
        <div className="container">
          <h1>О нас</h1>
          <p>
            Мы начали Skör, несмотря на скептицизм некоторых членов семьи и
            друзей, с верой в то, что узбекский народ пойдет на истинное
            качество и здоровый выбор, если ему будет предоставлен выбор.
            Конечно, наши соки могут быть более дорогими, чем концентраты - но
            это потому, что наши готовятся из свежих фруктов и овощей; используя
            технологию холодного прессования небольшими партиями. Соки холодного
            отжима сохраняют больше полезных для здоровья ферментов и витаминов,
            а также деликатный вкус ингредиентов. В этом смысл Skör,
            скандинавский для деликатного. Только лучшие ингредиенты, холодного
            отжима, полные витаминов, без искусственного вкуса и без добавления
            сахара!
          </p>
        </div>
      </section>
    </>
  );
}
