import React from "react";

export default function sok() {
  if (typeof window !== "undefined") {
    localStorage.setItem("rout", "/sok");
  }

  return (
    <div>
      <div className="mobile__sidebar">
        <div className="close">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30.406"
            height="30.406"
            viewBox="0 0 30.406 30.406"
          >
            <g
              id="Group_28"
              data-name="Group 28"
              transform="translate(-216.297 -8.672)"
            >
              <path
                id="Path_13"
                data-name="Path 13"
                d="M278,7h35"
                transform="translate(27.5 -190.025) rotate(45)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
              <path
                id="Path_14"
                data-name="Path 14"
                d="M0,0H35"
                transform="translate(243.874 11.5) rotate(135)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
            </g>
          </svg>
        </div>
      </div>
      <section className="product__main product__pages">
        <div className="container">
          <h1>Соки</h1>
          <div className="row">
            <div className="col-lg-3 col-sm-6">
              <div className="card">
                <img className="card-img-top" src="images/pr1.png" alt="" />
                <a href="#" className="card-title">
                  Яблочно - Лимонный
                </a>
                <p className="composition__product">
                  <b>Состав:</b> яблоко, лимон. Натуральный сок холодного
                  отжима. Без добавления сахара или воды.
                </p>
                <a href="#" className="btn__green">
                  <img src="images/cart__white.svg" alt="" />
                  UZS 7000
                </a>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="card">
                <img
                  className="card-img-top"
                  src="images/pr1.png"
                  alt="Card image cap"
                />
                <a href="#" className="card-title">
                  Яблочно - Лимонный
                </a>
                <p className="composition__product">
                  <b>Состав:</b> яблоко, лимон. Натуральный сок холодного
                  отжима. Без добавления сахара или воды.
                </p>
                <a href="#" className="btn__green">
                  <img src="images/cart__white.svg" alt="" />
                  UZS 7000
                </a>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="card">
                <img
                  className="card-img-top"
                  src="images/pr1.png"
                  alt="Card image cap"
                />
                <a href="#" className="card-title">
                  Яблочно - Лимонный
                </a>
                <p className="composition__product">
                  <b>Состав:</b> яблоко, лимон. Натуральный сок холодного
                  отжима. Без добавления сахара или воды.
                </p>
                <a href="#" className="btn__green">
                  <img src="images/cart__white.svg" alt="" />
                  UZS 7000
                </a>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="card">
                <img
                  className="card-img-top"
                  src="images/pr1.png"
                  alt="Card image cap"
                />
                <a href="#" className="card-title">
                  Яблочно - Лимонный
                </a>
                <p className="composition__product">
                  <b>Состав:</b> яблоко, лимон. Натуральный сок холодного
                  отжима. Без добавления сахара или воды.
                </p>
                <a href="#" className="btn__green">
                  <img src="images/cart__white.svg" alt="" />
                  UZS 7000
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
