import React from "react";

export default function Footer() {
  return (
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-lg-4 d-none d-lg-block">
            <h4>СТРАНИЦЫ</h4>
            <ul>
              <li>
                <a href="#">ГЛАВНАЯ</a>
              </li>
              <li>
                <a href="#">СОКИ</a>
              </li>
              <li>
                <a href="#">О НАС</a>
              </li>
              <li>
                <a href="#">КОНТАКТ</a>
              </li>
            </ul>
          </div>
          <div className="col-lg-4">
            <h4>ИНФОРМАЦИЯ</h4>
            <ul>
              <li>09:00-18:00</li>
              <li>
                <a href="tel:+998977772669">+998 97.777.2669</a>
              </li>
              <li>
                <a href="mailto:info@skor.com">info@skor.com</a>
              </li>
              <li>
                <a href="#">Zangiota tumani, Belariq ko’chasi 7-uy.</a>
              </li>
            </ul>
          </div>
          <div className="col-lg-4">
            <div className="social">
              <a href="#" className="instagramm">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="33"
                  height="33"
                  viewBox="0 0 33 33"
                >
                  <g
                    id="Icon_feather-instagram"
                    data-name="Icon feather-instagram"
                    transform="translate(1.5 1.5)"
                  >
                    <path
                      id="Path_10"
                      data-name="Path 10"
                      d="M10.5,3h15A7.5,7.5,0,0,1,33,10.5v15A7.5,7.5,0,0,1,25.5,33h-15A7.5,7.5,0,0,1,3,25.5v-15A7.5,7.5,0,0,1,10.5,3Z"
                      transform="translate(-3 -3)"
                      fill="none"
                      stroke="#707070"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="3"
                    />
                    <path
                      id="Path_11"
                      data-name="Path 11"
                      d="M24,17.055A6,6,0,1,1,18.945,12,6,6,0,0,1,24,17.055Z"
                      transform="translate(-3 -3)"
                      fill="none"
                      stroke="#707070"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="3"
                    />
                    <path
                      id="Path_12"
                      data-name="Path 12"
                      d="M26.25,9.75h0"
                      transform="translate(-3 -3)"
                      fill="none"
                      stroke="#707070"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="3"
                    />
                  </g>
                </svg>
              </a>
              <a href="#">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="34.875"
                  height="34.664"
                  viewBox="0 0 34.875 34.664"
                >
                  <path
                    id="Icon_awesome-facebook"
                    data-name="Icon awesome-facebook"
                    d="M35.438,18A17.438,17.438,0,1,0,15.275,35.227V23.041h-4.43V18h4.43V14.158c0-4.37,2.6-6.784,6.586-6.784a26.836,26.836,0,0,1,3.9.34V12h-2.2a2.52,2.52,0,0,0-2.841,2.723V18h4.836l-.773,5.041H20.725V35.227A17.444,17.444,0,0,0,35.438,18Z"
                    transform="translate(-0.563 -0.563)"
                    fill="#707070"
                  />
                </svg>
              </a>
              <a href="#">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="30"
                  height="24"
                  viewBox="0 0 30 24"
                >
                  <path
                    id="Icon_material-email"
                    data-name="Icon material-email"
                    d="M30,6H6A3,3,0,0,0,3.015,9L3,27a3.009,3.009,0,0,0,3,3H30a3.009,3.009,0,0,0,3-3V9A3.009,3.009,0,0,0,30,6Zm0,6L18,19.5,6,12V9l12,7.5L30,9Z"
                    transform="translate(-3 -6)"
                    fill="#707070"
                  />
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
