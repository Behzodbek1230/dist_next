/* eslint-disable @next/next/link-passhref */
/* eslint-disable @next/next/no-img-element */
import React from "react";
import Link from "next/link";
import Home from "../Home";
export default function Header() {
  return (
    <>
      <header>
        <div className="container">
          <div className="hdr__main">
            <Link href="/home">
              <a className="logo">
                <img src="images/logo.png" alt="" />
              </a>
            </Link>
            <ul>
              <li>
                <Link href="/home">
                  <a className="active"> ГЛАВНАЯ</a>
                </Link>
              </li>
              <li>
                <Link href="/sok">
                  <a>СОКИ</a>
                </Link>
              </li>
              <li>
                <Link href="/about">
                  <a>О НАС</a>
                </Link>
              </li>
              <li>
                <Link href="/contact">
                  <a>КОНТАКТ</a>
                </Link>
              </li>
              <li className="cart__li d-xl-none">
                <Link href="/card">
                  <a>КОРЗИНА</a>
                </Link>
              </li>
            </ul>
            <div className="lang">
              <Link href="#">
                <a className="active"> RU</a>
              </Link>
              <Link href="#">
                <a>UZ</a>
              </Link>
            </div>
            <Link href="/card">
              <a className="cart" data-product-cart="1">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="72"
                  height="72"
                  viewBox="0 0 72 72"
                >
                  <g
                    id="Cart_icon"
                    data-name="Cart icon"
                    transform="translate(-1353 -21)"
                  >
                    <g
                      id="Group_38"
                      data-name="Group 38"
                      transform="translate(391)"
                    >
                      <g
                        transform="matrix(1, 0, 0, 1, 962, 21)"
                        filter="url(#Rectangle_39)"
                      >
                        <rect
                          id="Rectangle_39-2"
                          data-name="Rectangle 39"
                          width="60"
                          height="60"
                          rx="30"
                          transform="translate(6 4)"
                          fill="#f0f0f0"
                        />
                      </g>
                      <path
                        id="Icon_awesome-shopping-cart"
                        data-name="Icon awesome-shopping-cart"
                        d="M29.436,16.794,32.07,5.2a1.338,1.338,0,0,0-1.3-1.634H8.874l-.511-2.5A1.338,1.338,0,0,0,7.052,0H1.338A1.338,1.338,0,0,0,0,1.338v.892A1.338,1.338,0,0,0,1.338,3.567h3.9L9.148,22.709a3.122,3.122,0,1,0,3.737.477H24.57a3.12,3.12,0,1,0,3.545-.58l.307-1.353a1.338,1.338,0,0,0-1.3-1.634H12.157l-.365-1.784H28.131A1.338,1.338,0,0,0,29.436,16.794Z"
                        transform="translate(981.198 40.731)"
                        fill="#707070"
                      />
                    </g>
                  </g>
                </svg>
              </a>
            </Link>
            <Link href="/login">
              <a className="btn__green">ВОЙТИ</a>
            </Link>
            <Link href="/register">
              <a className="register"> РЕГИСТРАЦИЯ</a>
            </Link>
            <div className="hamburger">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="39"
                height="28.921"
                viewBox="0 0 39 28.921"
              >
                <g
                  id="Group_27"
                  data-name="Group 27"
                  transform="translate(-285 -10)"
                >
                  <path
                    id="Path_8"
                    data-name="Path 8"
                    d="M278,7h35"
                    transform="translate(9 5)"
                    fill="none"
                    stroke="#707070"
                    strokeLinecap="round"
                    strokeWidth="4"
                  />
                  <path
                    id="Path_9"
                    data-name="Path 9"
                    d="M278,7h35"
                    transform="translate(9 17.46)"
                    fill="none"
                    stroke="#707070"
                    strokeLinecap="round"
                    strokeWidth="4"
                  />
                  <path
                    id="Path_10"
                    data-name="Path 10"
                    d="M278,7h35"
                    transform="translate(9 29.921)"
                    fill="none"
                    stroke="#707070"
                    strokeLinecap="round"
                    strokeWidth="4"
                  />
                  <path
                    id="Path_12"
                    data-name="Path 12"
                    d="M278,7h35"
                    transform="translate(9 5)"
                    fill="none"
                    stroke="#707070"
                    strokeLinecap="round"
                    strokeWidth="4"
                  />
                </g>
              </svg>
            </div>
          </div>
        </div>
      </header>
      <div className="mobile__sidebar">
        <div className="close">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30.406"
            height="30.406"
            viewBox="0 0 30.406 30.406"
          >
            <g
              id="Group_28"
              data-name="Group 28"
              transform="translate(-216.297 -8.672)"
            >
              <path
                id="Path_13"
                data-name="Path 13"
                d="M278,7h35"
                transform="translate(27.5 -190.025) rotate(45)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
              <path
                id="Path_14"
                data-name="Path 14"
                d="M0,0H35"
                transform="translate(243.874 11.5) rotate(135)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
            </g>
          </svg>
        </div>
      </div>
    </>
  );
}
