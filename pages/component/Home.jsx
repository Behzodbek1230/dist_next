/* eslint-disable @next/next/no-img-element */
import React from "react";
import Product from "./Product";
export default function Home() {
  if (typeof window !== "undefined") {
    localStorage.setItem("rout", "/home");
  }

  return (
    <>
      <section
        className="general"
        style={{ backgroundimage: "url('images/bc__main.png')" }}
      >
        <div className="swiper-container">
          <div className="swiper-wrapper">
            <div className="swiper-slide">
              <div className="container">
                <div className="row">
                  <div className="col-xl-6">
                    <h1>Прессованный</h1>
                    <p>Все свежее и натуральное</p>
                    <a href="#" className="btn__green">
                      Купить
                    </a>
                  </div>
                  <div className="col-xl-6">
                    <img src="images/round__product.png" alt="" />
                  </div>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div className="container">
                <div className="row">
                  <div className="col-xl-6">
                    <h1>Все свежее и натуральное</h1>
                    <p>Сок холодного отжима из свежих овощей и фруктов.</p>
                    <a href="#" className="btn__green">
                      Купить
                    </a>
                  </div>
                  <div className="col-xl-6">
                    <img src="images/round__product2.jpg" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="product__main">
        <div className="container">
          <h2>СОКИ</h2>
          <div className="row">
            <div className="col-lg-3 col-sm-6">
              <Product />
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="card">
                <img
                  className="card-img-top"
                  src="images/pr2.png"
                  alt="Card image cap"
                />
                <a href="#" className="card-title">
                  Гранатово - Яблочный
                </a>
                <p className="composition__product">
                  <b>Состав:</b> яблоко, лимон. Натуральный сок холодного
                  отжима. Без добавления сахара или воды.
                </p>
                <a href="#" className="btn__green">
                  <img src="images/cart__white.svg" alt="" />
                  UZS 7000
                </a>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <div className="card">
                <img
                  className="card-img-top"
                  src="images/pr3.png"
                  alt="Card image cap"
                />
                <a href="#" className="card-title">
                  Супер <br /> Зелёный
                </a>
                <p className="composition__product">
                  <b>Состав:</b> яблоко, лимон. Натуральный сок холодного
                  отжима. Без добавления сахара или воды.
                </p>
                <a href="#" className="btn__green">
                  <img src="images/cart__white.svg" alt="" />
                  UZS 7000
                </a>
              </div>
            </div>
            <div className="col-lg-3 col-sm-6">
              <Product />
            </div>
          </div>
          <h2>СОК ХОЛОДНОГО ОТЖИМА</h2>
          <div className="text">
            <p>
              Свежий, свежевыжатый,сырой и какая-то херня про свежевыжатый сок
              которую сложно понять. Он так же вызывает головную боль.
              Пожалуйста поясните параграф который на веб сайте. Тут должна быть
              чёткая информация о процессе холодного отжима сока.
            </p>
          </div>
          <h2>О НАС</h2>
          <p>
            Мы начали Skör, несмотря на скептицизм некоторых членов семьи и
            друзей, с верой в …
          </p>
          <a href="#" className="more">
            читать дальше
          </a>
        </div>
      </section>
    </>
  );
}
