/* eslint-disable @next/next/no-img-element */
import React from "react";

export default function Product() {
  return (
    <div className="card">
      <img className="card-img-top" src="images/pr1.png" alt="Card image cap" />
      <a href="#" className="card-title">
        Яблочно - Лимонный
      </a>
      <p className="composition__product">
        <b>Состав:</b> яблоко, лимон. Натуральный сок холодного отжима. Без
        добавления сахара или воды.
      </p>
      <a href="#" className="btn__green">
        <img src="images/cart__white.svg" alt="" />
        UZS 7000
      </a>
    </div>
  );
}
