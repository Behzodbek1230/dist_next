import "../styles/globals.css";
import { NextSeo } from "next-seo";
import Layout from "./component/Layout.jsx";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
// import Router from "next/router";
import DefaultLayout from "./component/DefaultLayout.jsx";
function MyApp({ Component, pageProps }) {
  const [t, sett] = useState(true);
  const router = useRouter();

  useEffect(() => {
    if (router.pathname == "/") {
      if (localStorage.getItem("rout")) {
        router.push(localStorage.getItem("rout"));
      } else router.push("/home");
    }
  });

  return (
    <Layout>
      <NextSeo
        title="Page Meta Title"
        description="This will be the page meta description"
        canonical="https://www.canonicalurl.ie/"
        openGraph={{
          url: "https://www.canonicalurl.ie/",
          title: "Open Graph Title",
          description: "Open Graph Description",
          images: [
            {
              url: "https://www.example.ie/og-image-01.jpg",
              width: 800,
              height: 600,
              alt: "Og Image Alt",
            },
            {
              url: "https://www.example.ie/og-image-02.jpg",
              width: 900,
              height: 800,
              alt: "Og Image Alt Second",
            },
            { url: "https://www.example.ie/og-image-03.jpg" },
            { url: "https://www.example.ie/og-image-04.jpg" },
          ],
        }}
      />
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;
