/* eslint-disable @next/next/no-img-element */
import React from "react";

export default function card() {
  if (typeof window !== "undefined") {
    localStorage.setItem("rout", "/card");
  }
  return (
    <div>
      <div className="mobile__sidebar">
        <div className="close">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30.406"
            height="30.406"
            viewBox="0 0 30.406 30.406"
          >
            <g
              id="Group_28"
              data-name="Group 28"
              transform="translate(-216.297 -8.672)"
            >
              <path
                id="Path_13"
                data-name="Path 13"
                d="M278,7h35"
                transform="translate(27.5 -190.025) rotate(45)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
              <path
                id="Path_14"
                data-name="Path 14"
                d="M0,0H35"
                transform="translate(243.874 11.5) rotate(135)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
            </g>
          </svg>
        </div>
      </div>
      <section className="cart">
        <div className="container">
          <h1>Корзина</h1>
          {/* <!-- if cart == empty --> */}
          <div className="empty__cart">
            <p>Ваша корзина пуста.</p>
            <a href="index.html" className="forget__password">
              Пополнить Корзину
            </a>
          </div>
          {/* <!-- if cart == empty --> */}
          <table>
            <thead>
              <tr>
                <th>Продукт</th>
                <th>Цена</th>
                <th>Кол.</th>
                <th>Итог</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <div className="cart__name">
                    <img src="images/pr1.png" alt="" />
                    <div className="text">
                      <h4>Яблочно - Лимонный</h4>
                      <a href="#">Удалить</a>
                    </div>
                  </div>
                </td>
                <td>
                  <div className="cart__price">UZS 9000</div>
                </td>
                <td>
                  <div className="d-sm-none">UZS 9000</div>
                  <div className="cart__count">
                    <button className="decrement">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                      >
                        <g
                          id="Group_124"
                          data-name="Group 124"
                          transform="translate(-1347 -586)"
                        >
                          <circle
                            id="Ellipse_4"
                            data-name="Ellipse 4"
                            cx="15"
                            cy="15"
                            r="15"
                            transform="translate(1347 586)"
                            fill="#707070"
                          />
                          <g
                            id="Group_123"
                            data-name="Group 123"
                            transform="translate(47)"
                          >
                            <path
                              id="Path_17"
                              data-name="Path 17"
                              d="M11428-1261v18"
                              transform="translate(63 -10827) rotate(90)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                          </g>
                        </g>
                      </svg>
                    </button>
                    <input type="number" defaultValue="1" min="1" />
                    <button className="increment">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                      >
                        <g
                          id="Group_125"
                          data-name="Group 125"
                          transform="translate(-1300 -586)"
                        >
                          <circle
                            id="Ellipse_3"
                            data-name="Ellipse 3"
                            cx="15"
                            cy="15"
                            r="15"
                            transform="translate(1300 586)"
                            fill="#707070"
                          />
                          <g id="Group_122" data-name="Group 122">
                            <path
                              id="Path_16"
                              data-name="Path 16"
                              d="M11428-1261v18"
                              transform="translate(-10113 1853)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                            <path
                              id="Path_17"
                              data-name="Path 17"
                              d="M11428-1261v18"
                              transform="translate(63 -10827) rotate(90)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                          </g>
                        </g>
                      </svg>
                    </button>
                  </div>
                </td>
                <td>UZS 9000</td>
              </tr>
              <tr>
                <td>
                  <div className="cart__name">
                    <img src="images/pr2.png" alt="" />
                    <div className="text">
                      <h4>Яблочно - Лимонный</h4>
                      <a href="#">Удалить</a>
                    </div>
                  </div>
                </td>
                <td>
                  <div className="cart__price">UZS 9000</div>
                </td>
                <td>
                  <div className="d-sm-none">UZS 9000</div>
                  <div className="cart__count">
                    <button className="decrement">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                      >
                        <g
                          id="Group_124"
                          data-name="Group 124"
                          transform="translate(-1347 -586)"
                        >
                          <circle
                            id="Ellipse_4"
                            data-name="Ellipse 4"
                            cx="15"
                            cy="15"
                            r="15"
                            transform="translate(1347 586)"
                            fill="#707070"
                          />
                          <g
                            id="Group_123"
                            data-name="Group 123"
                            transform="translate(47)"
                          >
                            <path
                              id="Path_17"
                              data-name="Path 17"
                              d="M11428-1261v18"
                              transform="translate(63 -10827) rotate(90)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                          </g>
                        </g>
                      </svg>
                    </button>
                    <input type="number" defaultValue="1" min="1" />
                    <button className="increment">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                      >
                        <g
                          id="Group_125"
                          data-name="Group 125"
                          transform="translate(-1300 -586)"
                        >
                          <circle
                            id="Ellipse_3"
                            data-name="Ellipse 3"
                            cx="15"
                            cy="15"
                            r="15"
                            transform="translate(1300 586)"
                            fill="#707070"
                          />
                          <g id="Group_122" data-name="Group 122">
                            <path
                              id="Path_16"
                              data-name="Path 16"
                              d="M11428-1261v18"
                              transform="translate(-10113 1853)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                            <path
                              id="Path_17"
                              data-name="Path 17"
                              d="M11428-1261v18"
                              transform="translate(63 -10827) rotate(90)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                          </g>
                        </g>
                      </svg>
                    </button>
                  </div>
                </td>
                <td>UZS 9000</td>
              </tr>
              <tr>
                <td>
                  <div className="cart__name">
                    <img src="images/pr3.png" alt="" />
                    <div className="text">
                      <h4>Яблочно - Лимонный</h4>
                      <a href="#">Удалить</a>
                    </div>
                  </div>
                </td>
                <td>
                  <div className="cart__price">UZS 9000</div>
                </td>
                <td>
                  <div className="d-sm-none">UZS 9000</div>
                  <div className="cart__count">
                    <button className="decrement">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                      >
                        <g
                          id="Group_124"
                          data-name="Group 124"
                          transform="translate(-1347 -586)"
                        >
                          <circle
                            id="Ellipse_4"
                            data-name="Ellipse 4"
                            cx="15"
                            cy="15"
                            r="15"
                            transform="translate(1347 586)"
                            fill="#707070"
                          />
                          <g
                            id="Group_123"
                            data-name="Group 123"
                            transform="translate(47)"
                          >
                            <path
                              id="Path_17"
                              data-name="Path 17"
                              d="M11428-1261v18"
                              transform="translate(63 -10827) rotate(90)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                          </g>
                        </g>
                      </svg>
                    </button>
                    <input type="number" defaultValue="1" min="1" />
                    <button className="increment">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                      >
                        <g
                          id="Group_125"
                          data-name="Group 125"
                          transform="translate(-1300 -586)"
                        >
                          <circle
                            id="Ellipse_3"
                            data-name="Ellipse 3"
                            cx="15"
                            cy="15"
                            r="15"
                            transform="translate(1300 586)"
                            fill="#707070"
                          />
                          <g id="Group_122" data-name="Group 122">
                            <path
                              id="Path_16"
                              data-name="Path 16"
                              d="M11428-1261v18"
                              transform="translate(-10113 1853)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                            <path
                              id="Path_17"
                              data-name="Path 17"
                              d="M11428-1261v18"
                              transform="translate(63 -10827) rotate(90)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                          </g>
                        </g>
                      </svg>
                    </button>
                  </div>
                </td>
                <td>UZS 9000</td>
              </tr>
              <tr>
                <td>
                  <div className="cart__name">
                    <img src="images/pr1.png" alt="" />
                    <div className="text">
                      <h4>Яблочно - Лимонный</h4>
                      <a href="#">Удалить</a>
                    </div>
                  </div>
                </td>
                <td>
                  <div className="cart__price">UZS 9000</div>
                </td>
                <td>
                  <div className="d-sm-none">UZS 9000</div>
                  <div className="cart__count">
                    <button className="decrement">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                      >
                        <g
                          id="Group_124"
                          data-name="Group 124"
                          transform="translate(-1347 -586)"
                        >
                          <circle
                            id="Ellipse_4"
                            data-name="Ellipse 4"
                            cx="15"
                            cy="15"
                            r="15"
                            transform="translate(1347 586)"
                            fill="#707070"
                          />
                          <g
                            id="Group_123"
                            data-name="Group 123"
                            transform="translate(47)"
                          >
                            <path
                              id="Path_17"
                              data-name="Path 17"
                              d="M11428-1261v18"
                              transform="translate(63 -10827) rotate(90)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                          </g>
                        </g>
                      </svg>
                    </button>
                    <input type="number" defaultValue="1" min="1" />
                    <button className="increment">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 30 30"
                      >
                        <g
                          id="Group_125"
                          data-name="Group 125"
                          transform="translate(-1300 -586)"
                        >
                          <circle
                            id="Ellipse_3"
                            data-name="Ellipse 3"
                            cx="15"
                            cy="15"
                            r="15"
                            transform="translate(1300 586)"
                            fill="#707070"
                          />
                          <g id="Group_122" data-name="Group 122">
                            <path
                              id="Path_16"
                              data-name="Path 16"
                              d="M11428-1261v18"
                              transform="translate(-10113 1853)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                            <path
                              id="Path_17"
                              data-name="Path 17"
                              d="M11428-1261v18"
                              transform="translate(63 -10827) rotate(90)"
                              fill="none"
                              stroke="#fff"
                              strokeWidth="3"
                            />
                          </g>
                        </g>
                      </svg>
                    </button>
                  </div>
                </td>
                <td>UZS 9000</td>
              </tr>
            </tbody>
          </table>
          <div className="total__cart">
            <p>
              <span>ИТОГ:</span>UZS 24,000
            </p>
            <a href="payment.html" className="btn__green">
              Оплатить
            </a>
          </div>
        </div>
      </section>
    </div>
  );
}
