/* eslint-disable @next/next/no-css-tags */
import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const originalRenderPage = ctx.renderPage;

    // Run the React rendering logic synchronously
    ctx.renderPage = () =>
      originalRenderPage({
        // Useful for wrapping the whole react tree
        enhanceApp: (App) => App,
        // Useful for wrapping in a per-page basis
        enhanceComponent: (Component) => Component,
      });

    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx);

    return initialProps;
  }

  render() {
    return (
      <Html>
        <Head>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <meta
            name="description"
            content="Complete description of the content showed in this sample page."
          />
          <meta property="og:title" content="My Sample Page" />
          <meta
            property="og:description"
            content="Complete description of the content showed in this sample page for Open Graph."
          />
          <meta
            property="og:url"
            content="http://nextjs-test.kibera-soft.uz/home"
          />
          <meta property="og:type" content="website" />

          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap"
            rel="stylesheet"
          />
          <link rel="stylesheet" href="fonts/stylesheet.css" />
          <link rel="stylesheet" href="css/bootstrap.min.css" />
          <link rel="stylesheet" href="css/swiper.min.css" />
          <link rel="stylesheet" href="css/style.css" />
        </Head>
        <body>
          <Main />
          <NextScript src="js/jquery-3.5.1.min.js" />
          <NextScript src="js/bootstrap.min.js" />
          <NextScript src="js/swiper.min.js" />
          <NextScript src="js/main.js" />
        </body>
        <NextScript />
      </Html>
    );
  }
}

export default MyDocument;
