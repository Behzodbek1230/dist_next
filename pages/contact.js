import React from "react";

export default function contact() {
  if (typeof window !== "undefined") {
    localStorage.setItem("rout", "/contact");
  }
  return (
    <div>
      {" "}
      <div className="mobile__sidebar">
        <div className="close">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30.406"
            height="30.406"
            viewBox="0 0 30.406 30.406"
          >
            <g
              id="Group_28"
              data-name="Group 28"
              transform="translate(-216.297 -8.672)"
            >
              <path
                id="Path_13"
                data-name="Path 13"
                d="M278,7h35"
                transform="translate(27.5 -190.025) rotate(45)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
              <path
                id="Path_14"
                data-name="Path 14"
                d="M0,0H35"
                transform="translate(243.874 11.5) rotate(135)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
            </g>
          </svg>
        </div>
      </div>
      <section className="contact">
        <div className="container">
          <h1>Контакт</h1>
          <div className="row">
            <div className="col-lg-6">
              <form action="#" className="form__style">
                <div className="form-group bord success">
                  <label htmlFor="name">Имя:</label>
                  <input type="text" id="name" defaultValue="Иван Драго" />
                </div>
                <div className="form-group bord success">
                  <label htmlFor="email">Email:</label>
                  <input
                    type="text"
                    id="email"
                    defaultValue="ivandrago@gmail.com"
                  />
                </div>
                <div className="form-group bord success">
                  <label htmlFor="te">Тема письма:</label>
                  <input type="text" id="te" defaultValue="Крупные поставки" />
                </div>
                <div className="form-group errored">
                  <textarea name="" id="" placeholder="Сообщение:"></textarea>
                </div>
                <button className="btn__green" type="submit">
                  Отправить
                </button>
              </form>
            </div>
            <div className="col-lg-6">
              <div className="contact_right">
                <p>
                  <b>Часы работы:</b> 09:00-18:00
                </p>
                <p>
                  <b>Тел.</b> <a href="tel:+998977772669">+998 97.777.2669</a>
                </p>
                <p>
                  <b>Эл.почта:</b>
                  <a href="mailto: info@skor.com">info@skor.com</a>
                </p>
                <p>
                  <b>Адрес:</b> Zangiota tumani, Belariq ko’chasi 7-uy.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
