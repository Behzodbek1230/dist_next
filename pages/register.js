import React from "react";

export default function register() {
  if (typeof window !== "undefined") {
    localStorage.setItem("rout", "/register");
  }
  return (
    <div>
      <div className="mobile__sidebar">
        <div className="close">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30.406"
            height="30.406"
            viewBox="0 0 30.406 30.406"
          >
            <g
              id="Group_28"
              data-name="Group 28"
              transform="translate(-216.297 -8.672)"
            >
              <path
                id="Path_13"
                data-name="Path 13"
                d="M278,7h35"
                transform="translate(27.5 -190.025) rotate(45)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
              <path
                id="Path_14"
                data-name="Path 14"
                d="M0,0H35"
                transform="translate(243.874 11.5) rotate(135)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
            </g>
          </svg>
        </div>
      </div>
      <section className="register">
        <div className="circle"></div>
        <div className="container">
          <h1>Регистрация</h1>
          <div className="row">
            <div className="col-xl-6">
              <form
                action="register_sms_input.html"
                className="form__style register__form"
              >
                <div className="form-group">
                  <label htmlFor="name">И.Ф.О.</label>
                  <input
                    type="text"
                    id="name"
                    defaultValue="Иван Драго Драгонов"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="email">Email:</label>
                  <input
                    type="text"
                    id="email"
                    defaultValue="ivandrago@gmail.com"
                  />
                </div>
                <div className="form-group mb-5">
                  <label htmlFor="">Тел:</label>
                  <input type="tel" defaultValue="1234567890987654321" />
                </div>
                <div className="form-group">
                  <label htmlFor="">Адрес:</label>
                  <input type="text" defaultValue="1289 какая-то Ул. кв12873" />
                </div>
                <div className="row">
                  <div className="col-lg-7">
                    <div className="form-group mb-5">
                      <label htmlFor="">Город:</label>
                      <input type="text" defaultValue="Ташкент" />
                    </div>
                  </div>
                  <div className="col-lg-5">
                    <div className="form-group mb-5">
                      <label htmlFor="">Индекс:</label>
                      <input type="text" defaultValue="100012" />
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="">Пароль:</label>
                  <input
                    type="password"
                    defaultValue="********************************"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="">Повторитe пароль:</label>
                  <input
                    type="password"
                    defaultValue="********************************"
                  />
                </div>
                <div className="privacy__block checkbox__style mb-4 mb-md-0">
                  <input type="checkbox" id="uu1" />
                  <label htmlFor="uu1">
                    I agree with <a href="#">Privacy Policy</a>
                  </label>
                </div>
                <button className="btn__green" type="submit">
                  Продолжить
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
