import React from "react";

export default function login() {
  if (typeof window !== "undefined") {
    localStorage.setItem("rout", "/login");
  }
  return (
    <div>
      <div className="mobile__sidebar">
        <div className="close">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30.406"
            height="30.406"
            viewBox="0 0 30.406 30.406"
          >
            <g
              id="Group_28"
              data-name="Group 28"
              transform="translate(-216.297 -8.672)"
            >
              <path
                id="Path_13"
                data-name="Path 13"
                d="M278,7h35"
                transform="translate(27.5 -190.025) rotate(45)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
              <path
                id="Path_14"
                data-name="Path 14"
                d="M0,0H35"
                transform="translate(243.874 11.5) rotate(135)"
                fill="none"
                stroke="#000"
                strokeLinecap="round"
                strokeWidth="4"
              />
            </g>
          </svg>
        </div>
      </div>
      <section className="register">
        <div className="circle"></div>
        <div className="container">
          <h1>Войти</h1>
          <div className="row">
            <div className="col-xl-6">
              <form
                action="register_sms_input.html"
                className="form__style register__form"
              >
                <div className="form-group mb-5">
                  <label htmlFor="">Тел:</label>
                  <input type="tel" defaultValue="+998" placeholder="+998" />
                </div>
                <div className="form-group">
                  <label htmlFor="">Пароль:</label>
                  <input
                    type="password"
                    defaultValue="********************************"
                  />
                </div>
                <div className="d-flex align-items-center justify-content-between mb-4">
                  <div className="privacy__block checkbox__style">
                    <input type="checkbox" id="uu1" />
                    <label htmlFor="uu1">Запомнить меня</label>
                  </div>
                  <a href="#" className="forget__password">
                    Забыли Пароль?
                  </a>
                </div>
                <button className="btn__green" type="submit">
                  Войти
                </button>
                <div className="text-right">
                  <a href="register.html" className="to__register">
                    Зарегистрироваться
                  </a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
