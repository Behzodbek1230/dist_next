/** @type {import('next').NextConfig} */
const nextConfig = {
  // trailingSlash: true,

  reactStrictMode: true,
  images: {
    loader: "imgix",
    path: "",
  },
};

module.exports = nextConfig;
