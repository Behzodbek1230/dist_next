$(document).ready(function () {
  $(document).on("click", ".to1", function (e) {
    e.preventDefault();
    $("a#thirsd-tab").trigger("click");
  });

  $(document).on("click", ".to2", function (e) {
    e.preventDefault();
    $("a#fourth-tab").trigger("click");
  });

  $(".profile__page .collapse").collapse({
    toggle: false,
  });

  const swiper = new Swiper(".general .swiper-container", {
    speed: 800,
    spaceBetween: 100,
    // effect: 'fade',
    loop: true,
    slidesPerView: 1,
    autoplay: {
      delay: 5000,
    },
  });

  if ($(window).width() < 1300) {
    $(".mobile__sidebar .close").after($("header .hdr__main a.register"));
    $(".mobile__sidebar .close").after($("header .hdr__main a.btn__green"));
    $(".mobile__sidebar .close").after($("header .hdr__main ul"));
  }

  $(document).on("click", "header .hamburger", function () {
    $("body").addClass("open__menu");
  });

  $(document).on(
    "click",
    ".mobile__sidebar .close, .mobile__sidebar .btn__green, .mobile__sidebar .register",
    function () {
      $("body").removeClass("open__menu");
    }
  );

  // $(document).on('click', '.hamburger', function() {
  //     $('body').toggleClass('menu')
  // })

  // $(document).on('click', 'body', function(e) {
  //     if (!$(e.target).is('.hamburger, .hamburger *, .sidebar__header main, .sidebar__header main *')) {
  //         $('body').removeClass('menu')

  //     }
  // })

  // $(document).on('click', 'body', function(e){
  //     if (!$(e.target).is('.select__main .top *, .select__main .top')) {
  //         $('.select__main').removeClass('opened')
  //     }
  // })
});
